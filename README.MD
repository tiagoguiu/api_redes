> # NODE.JS SERVER PARA RECEBER DADOS DO APP DE INICIAÇÃO CIENTIFICA

+ Utilizando MongoDB, Express e HTTPs

>## FUNÇÕES DO SERVER:
+ Receber dados via api rest
+ Salvar no banco de dados Mongo
+ Log de Dados Salvos

> ## PRÓXIMAS ADIÇÕES:
+ Receptação de mais campos vindos do flutter
+ Manipulação dos dados para geração de informação para provedor

> ## TÉCNOLOGIAS USADAS:
<table>
    <tr>
        <td>JAVASCRIPT</td>
        <td>NODE.JS</td>
        <td>MONGODB</td>
        <td>EXPRESS</td>
        <td>HTTPS/SSL</td>
    </tr>
    <tr>
        <td>ECMASCRIPT 2018</td>
        <td>14.17.1</td>
        <td>4.4.6</td>
        <td>4.**</td>
        <td>1.**</td>
    </tr>

</table>