const express = require('express');

const mongoose = require('mongoose');

const fs = require('fs');

const path = require('path')

const https = require('https')

const http = require('http')

const app = express();

// Body Parser Middleware
app.use(express.json());

const options = process.env.OPTIONS
//tinha  um _dirname antes de cert

//ROUTES
const postsRoutes = require('./routes/api/posts');

require('dotenv').config()


mongoose.connect(process.env.MONGO_URL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }).then(() => console.log('MongoDB conectado!')).catch(err => console.log(err));


//USER ROUTES
app.use('/api/posts', postsRoutes);
//PARA USAR A ROTA BASTA ACESSAR LOCALHOST:3000/API/POSTS


const PORT = process.env.PORT;

const server = http.createServer(app);

server.listen(PORT, () => {
    console.log('Servidor https ativo na porta 3000');
});




//app.listen(PORT, ()=>console.log("Servidor http rodando porta 4000"))


//PARA DEPLOY NO HEROKU É NECESSARIO CRIAR UM CLUSTER MONGODB
//VISTO QUE O HEROKU NÃO PERMITE CRIAR A BASE DE DADOS