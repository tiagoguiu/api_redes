const express = require('express');

const router = express.Router();
// post model
const Posts = require('../../models/Posts');

// @routes GET api/posts
// @description pegar todos posts
//UTILIZANDO A MESMA ROTA APENAS COM O METODO GET 
router.get('/', async (req,res) => {
    try{
        const posts = await Posts.find();
        if(!posts) throw Error('Nenhum item');
        res.status(200).json(posts);
    } catch(err){
        res.status(400).json({ msg : err});
    }
}); 


// @routes POST api/posts
// @description criar um post

router.post('/', async (req,res) => {
    console.log(req.body);

    const newPost = new Posts(req.body);

    try{
        const post = await newPost.save();
        if(!post) throw Error('Algo deu errado enquanto era salvo no banco de dados');

        res.status(200).json(post);
    } catch(err){

        res.status(400).json({ msg : err});
    }
});


// @routes DELETE api/posts
// @description deletar algum post
//UTILIZANDO A MESMA ROTA APENAS COM O METODO DELETE
//PARA ISSO DEVERÁ SER COPIADO O ID DO OBJETO A SER DELETADO
//E COLOCADO COMO PARAMETRO APÓS API/POSTS/OPARAMETRO 
router.delete('/', async (req,res) => {
    try{
        const post = await Posts.findByIdAndDelete(req.params.id);
        if(!post) throw Error('Nenhum post encontrado');

        res.status(200).json({success: true});
    } catch(err){
        res.status(400).json({ msg : err});
    }
}); 


// @routes UPDATE api/posts
// @description atualizar um post
//UTILIZANDO A MESMA ROTA APENAS COM O METODO UPDATE
//MESMO ESQUEMA DO METODO DELETE, SENDO UTILIZADO O ID COMO PARAMETRO 
router.patch('/:id', async (req,res) => {
    try{
        const post = await Posts.findByIdAndUpdate(req.params.id, req.body);
        if(!posts) throw Error('Não foi possivel atualizar');

        res.status(200).json({ success : true});
    } catch(err){
        res.status(400).json({ msg : err});
    }
}); 

// @routes GET api/posts/:id
// @description pegar UM posts
//UTILIZANDO A MESMA ROTA APENAS COM O METODO GET
//COM O ID COMO PARAMETRO 
router.get('/', async (req,res) => {
    try{
        const posts = await Posts.findById(req.params.id);
        if(!post) throw Error('Nenhum item');
        res.status(200).json(post);
    } catch(err){
        res.status(400).json({ msg : err});
    }
}); 



module.exports = router;