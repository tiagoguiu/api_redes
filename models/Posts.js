const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    ip: {
        type: String,
        required: true,
    },
    macdev: {
        type: String,
        required: true,
    },
    routerIp: {
        type: String,
        required: true,
    },
    frequency: {
        type: String,
        required: true,
    },
    signal: {
        type: String,
        required: true,
    },
    submask: {
        type: String,
        required: true,
    },
    ssid: {
        type: String,
        required: true,
    },
    ipv6: {
        type: String,
        required: true,
    },
    bssid: {
        type: String,
        required: true,
    },
    broadcast: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('Posts', PostSchema);